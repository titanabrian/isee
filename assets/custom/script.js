$(document).ready(function() {
   $('#example').DataTable({
      responsive: true ,
      dom: 'Bfrtip',
      buttons: [
         'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });
   $('#example1').DataTable({
    responsive : true
   }); 
   $('#example2').DataTable({
    responsive : true
   }); 
});

$(document).ready(function(){
  $('#form1').validate({
    rules: {
      fullname: {
        required : true,
        digits : false,
        minlength : 4
      },
      name: {
        required : true,
        digits : false,
      },
      email: {
        required : true,
        email : true
      },  
      password: {
        minlength : 8
      },
      passwordconfirm: {
        minlength : 8,
        equalTo : "#password"
      },

    },

    messages: {
      fullname : {
        required : "field ini harus diisi",
        digits : "field tak boleh diisi oleh angka",
        minlength : "minimum harus 4 huruf"
      },
      name : {
        required : "field ini harus diisi",
        digits : "field tak boleh diisi oleh angka",
      },
      email : {
        required : "email harus diisi",
        email : "email harus sesuai format"
      },
      password : {
        minlength : "password harus minimal 8 karakter"
      },
      passwordconfirm : {
        minlength : "password harus minimal 8 karakter",
        equalTo : "password tidak sesuai"
      },
    },
  });

  $('#form2').validate({
    rules : {
      fullname : {
        required : true,
        minlength : 4
      },
      nim : {
        required : true,
        digits : true,
        rangelength :[14,14]
      },
    },

    messages : {
      fullname : {
        required : "field ini harus diisi",
        minlength : "panjang nama minimal 4 huruf"
      },
      nim : {
        required : "field ini harus diisi",
        digits : "masukan hanya berupa angka",
        rangelength : "panjang karakter harus 14 huruf"
      },
    },
  });

  $('#form3').validate({
    rules : {
      fullname : {
        required : true,
        minlength : 4
      },
      email : {
        required : true,
        email : true
      },
      password : {
        minlength : 8
      },
      passwordconfirm : {
        minlength : 8,
        equalTo : "#password"
      },
    },

    messages : {
      fullname : {
        required : "field ini harus diisi",
        minlength : "panjang nama minimal 4 huruf"
      },
      email : {
        required : "field ini harus diisi",
        email : "format email anda salah"
      },
      password :{
        minlength : "password harus terisi paling sedikit 8 karakter"
      },
      passwordconfirm :{
        minlength : "password harus terisi paling sedikit 8 karakter",
        equalTo : "password tidak cocok"
      },
    },
  });

   $('#form4').validate({
    rules: {
      fullname: {
        required : true,
        digits : false,
        minlength : 4
      },
      name: {
        required : true,
        digits : false,
      },
      email: {
        required : true,
        email : true
      },  
      password: {
        required : true,
        minlength : 8
      },
      passwordconfirm: {
        required : true,
        minlength : 8,
        equalTo : "#password"
      },

    },

    messages: {
      fullname : {
        required : "field ini harus diisi",
        digits : "field tak boleh diisi oleh angka",
        minlength : "minimum harus 4 huruf"
      },
      name : {
        required : "field ini harus diisi",
        digits : "field tak boleh diisi oleh angka",
      },
      email : {
        required : "email harus diisi",
        email : "email harus sesuai format"
      },
      password : {
        required : "password harus diisi",
        minlength : "password harus minimal 8 karakter"
      },
      passwordconfirm : {
        required : "password harus diisi",
        minlength : "password harus minimal 8 karakter",
        equalTo : "password tidak sesuai"
      },
    },
  });
  // $('#modalcontoh').on('hidden.bs.modal', function() {
  //   var $alertas = $('#formcontoh');
  //   $alertas.validate().resetForm();
  //   $alertas.find('.error').removeClass('error');
  // });

  // $('#modaledit-admin').on('show.bs.modal', function (e) {
  //   var rowid = $(e.relatedTarget).data('id');
  //   //menggunakan fungsi ajax untuk pengambilan data
  //   $.ajax({
  //     type : 'post',
  //     url : base_url('?action=server'),
  //     data :  'rowid='+ rowid,
  //     success : function(data){
  //       $('.ambil-data').html(data);//menampilkan data ke dalam modal
  //     }
  //   });
  // });


  // $('#modaledit-admin').on('hidden.bs.modal', function() {
  //   var $alertas = $('#form1');
  //   $alertas.validate().resetForm();
  //   $alertas.find('.error').removeClass('error');
  // });

});


$(document).ready(function(){
  setTimeout(function(){
        $("#fade-out").fadeOut("slow");
    },1000)
});

$(document).ready(function(){
    $("#btn3").click(function(){
        $(".list-group").append('<li class="list-group-item"><span><div class="form-group"><input class="form-control form-custom" type="text" name="jawaban[]" id="jawaban" placeholder="Isikan jawaban disini"></div><button class="btn btn-danger" onclick="hapus_jawaban(this)">Hapus</button></span></li>');
    	if ($(".list-group-item").length > 0){
			$('.custom-select').prop("disabled",false);
		}
    });
    
});

function hapus_jawaban(elem){
    $(elem).parents("li").remove();
    if ($(".list-group-item").length < 1){
			$('#custom-select').prop("disabled",true);
	}
}


$(document).ready(function () {
       $('#sidebarCollapse').on('click', function () {
       	$('#sidebar').toggleClass('active');
        $('#custom1').toggleClass('col-md-12 col-sm-12 col-xs-12');
        $('#custom2').toggleClass('col-md-12 col-sm-12 col-xs-12');
	});
});

// $(document).ready(function(){
//     $(document).on('submit','form#contoh',function(e){
//         e.preventDefault(); 
//         $form = $(this);
//         uploadImage($form);
//     });

//     function uploadImage($form){

//       $form.find('.progress-bar').removeClass('bg-success')
//                               .removeClass('bg-danger');
                                    
//       var formdata = new FormData($form[0]);
//       var request = new XMLHttpRequest();


//       //progress event  
//       request.upload.addEventListener('progress',function(e){
//         var persen = Math.round(e.loaded/e.total * 100);
//         $form.find('.progress-bar').width(persen+'%').html(persen+'%');
//       });

//       //progress completed load event 
//       request.addEventListener('load',function(e){
//         $form.find('.progress-bar').addClass('bg-success').html('pengunggahan selesai ....');
//       });

//       request.open('post',base_url('?action=server'));
//       request.send(formdata);

//       $form.on('click','.cancel',function(){
//         request.abort();
//         $form.find('.progress-bar')
//             .addClass('bg-danger')
//             .removeClass('bg-success')
//             .html('pengunggahan dibatalkan.....');
//       });
//     }
// });