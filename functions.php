<?php

	function base_url($str = ""){
		return "http://localhost/isee/".$str;
	}

	function test_input($data){
		$data=trim($data);
		$data=stripslashes($data);
		$data=htmlspecialchars($data);
		return $data;
	}

	function informasi($tipe="", $text=""){
		return "<div class='alert alert-".$tipe."' id='fade-out'>".$text."</div>";
	}

	function infoadvance($tipe="",$text=""){
		return "<div class='alert alert-".$tipe." alert-dismissible fade show' role='alert' id='fade-out'>".$text."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
	}

	function ambil_template($template = "", $views = ""){
		include_once($template);
	}

	function alert($msg) {
    	echo "<script type='text/javascript'>alert('$msg');</script>";
	}