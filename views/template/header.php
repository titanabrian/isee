<!DOCTYPE html>
<html>
<head>
	<title><?= !empty($title) ? $title." - " : "Suarakita | Masukkan suaramu disini !"; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">

	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/custom/datatables.min.css'); ?>">
	<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.min.css"> -->
	
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/custom/Google-Style-Login.css');?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/custom/style.css'); ?>">
</head>
<body>