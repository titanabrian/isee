	<script type="text/javascript">function base_url(str){
		return "<?= base_url(''); ?>"+str;
	}</script>
	<script type="text/javascript" src='<?= base_url('assets/jquery/jquery-3.2.1.min.js'); ?>'></script>
	<script src='<?= base_url('assets/jquery/jquery.validate.min.js'); ?>'></script>
	<script src='<?= base_url('assets/jquery/popper.js'); ?>'></script>
	<script src='<?= base_url('assets/bootstrap/js/bootstrap.min.js'); ?>'></script>
	<script src='<?= base_url('assets/jquery/pdfmake.min.js'); ?>'></script>
	<script src='<?= base_url('assets/jquery/vfs_fonts.js'); ?>'></script>
	<script src='<?= base_url('assets/jquery/datatables.min.js'); ?>'></script>
	<script src='<?= base_url('assets/jquery/canvas.min.js'); ?>'></script>
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
	<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/kt-2.3.2/r-2.2.0/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.3/datatables.min.js"></script> -->
	<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
	<script src='<?= base_url('assets/custom/script.js'); ?>'></script>
</body>
</html>