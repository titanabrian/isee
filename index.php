<?php
		session_start();
    include 'functions.php';
    if(!isset($_SESSION['login'])){
    	echo "<script>window.location.href ='login_user.php'</script>";
    }
?>
<!DOCTYPE html>
<html>
<head>
	 <style>
      #map {
        height: 400px;
        width: 100%;
      }
  	</style>
	<title>HISTORY</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url('/assets/bootstrap/css/bootstrap.min.css')?>">
	<script type="text/javascript" src="<?=base_url('/assets/custom/script.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('/assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASEsW7uBN1cKvzhbeRouRnPdacaAXkpgk&callback=initMap&libraries=places">
    </script>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand " href="#">ISEE</a>
			</div>
			<ul class="nav navbar-nav"></ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="logout.php"><span class="glyphicon glyphicon-log-out" id="btn-logout"></span>Logout</a></li>
			</ul>
		</div>
	</nav>
	<div class="container" align="middle">
		<h2>YOUR BOCAH LOCATION</h2>
		<input hidden type="text" name="" id="longitude" value="">
	    <input hidden type="text" name="" id="latitude" value="">
	    <input type="" hidden  name="email" id="hidden-email" value="<?=$_SESSION['email']?>">
	    <button type="button" id="btn-get" name="button" class="btn btn-info">Get Location</button><br/>
	    <br/>
	    <div id="map"></div>
	</div>
	<div class="footer"></div>
	<script type="text/javascript">
		function initMap() {
        var lat = parseFloat(document.getElementById("latitude").value);
        var lang = parseFloat(document.getElementById("longitude").value);

        var uluru = {lat: lat, lng: lang};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
			}
      $(document).ready(function(){
      	var ee = $('#hidden-email').val();
      	// alert(ee);
        $('#btn-get').click(function(){
          $.ajax({
            type: "POST",
            url: "get_location.php",
            data: {email:ee},
            dataType:'json',
            success: function(data){
              $('#longitude').val(data.longitude);
              $('#latitude').val(data.latitude);
              initMap();
            }
          });
        });
      });
	</script>

</body>
</html>
