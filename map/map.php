<!DOCTYPE html>
<html>
  <head>
    <style>
      #map {
        height: 400px;
        width: 100%;
       }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  </head>
  <body>
    <h3>My Google Maps Demo</h3>
    <input hidden type="text" name="" id="longitude" value="">
    <input hidden type="text" name="" id="latitude" value="">
    <button type="button" id="btn-get" name="button">Get Location</button>
    <br/>
    <div id="map"></div>
    <script>
      function initMap() {
        var lat = parseFloat(document.getElementById("latitude").value);
        var lang = parseFloat(document.getElementById("longitude").value);

        var uluru = {lat: lat, lng: lang};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
      $(document).ready(function(){
        $('#btn-get').click(function(){
          $.ajax({
            type: "POST",
            url: "get_location.php",
            data: {},
            dataType:'json',
            success: function(data){
              $('#longitude').val(data.longitude);
              $('#latitude').val(data.latitude);
              initMap();
            },
          });
        });
      });


    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASEsW7uBN1cKvzhbeRouRnPdacaAXkpgk&callback=initMap">
    </script>
  </body>
</html>
