<?php
    include 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>ISEE - Login Page</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url('/assets/bootstrap/css/bootstrap.min.css')?>">
    <script type="text/javascript" src="<?=base_url('/assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('/assets/custom/Google-Style-Login.css')?>">
    <script type="text/javascript" src="<?=base_url('/assets/custom/script.js')?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
    <div class="login-card" align="middle" >
        <p class="profile-name-card"> </p>
        <form class="form-signin" action="process.php" method="POST"><span class="reauth-email" > </span>
             <?= !empty($error) ? $error : ""; ?>
             <div class="title">
             <h5>MASUK</h5>
            </div>
            <input type="hidden" name="csrf_token" value="<?= $csrf_value; ?>"> 
            <div class='form-group'>
            <label>Email</label>
            <input class="form-control" type="text"  autofocus="" id="email" name="email">
            </div>

            <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="password"  name="password" id="password">
            </div>
            <button class="btn btn-primary" type="submit" name="login" id="btn-login">Log in</button>
        </form>  
    </div>

</body>
<script type="text/javascript">
    $(document).ready(function(){
        // $('#btn-login').click(function(){
        //     var uname = $('#username').val();
        //     var pwd = $('#password').val();
        //     $.ajax({
        //         method : "post",
        //         url : "process.php",
        //         data : {username : uname, password : pwd},
        //         success : function(data){

        //         }
        //     });
        // });
    });
</script>
</html>

<!--     <div class="login-card"><img align='middle' width='200' class="profile-img-card">
        <p class="profile-name-card"> </p>
        <form class="form-signin" method="POST"><span class="reauth-email" > </span>
             <?= !empty($error) ? $error : ""; ?>
             <div class="title">
             <h5>MASUK</h5>
            </div>
            <input type="hidden" name="csrf_token" value="<?= $csrf_value; ?>"> 
            <div class='form-group'>
            <label>Username</label>
            <input class="form-control" type="text"  autofocus="" id="inputEmail" name="username">
            </div>

            <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="password"  name="password" id="inputPassword">
            </div>
            <button class="btn btn-primary" type="submit" name="login">Log in</button>
        </form>
    </div>
 -->